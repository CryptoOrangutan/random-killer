class Press {
  handleEvent(event) {
    let waitingForClick = false;
    switch (event.type) {
      case 'click':
        waitingForClick = setTimeout(function () {
          playGame();
        }, 500);
        break;
    }
  }
}

let press = new Press();
const playButton = document.getElementById('play-button-a');
playButton.addEventListener('click', press);

function playGame() {
  let players = document.getElementById("count").value;

  const max = Number(players);
  if (max === 0) {
    alert("Введите количество игроков!");
    return;
  }

  let resultDiv = document.getElementById("result");
  resultDiv.value = "Происходит вычисление неудачника...";

  let splash = setInterval(function () {
    resultDiv.style.opacity = 1 - (resultDiv.style.opacity || 1);
  }, 600);

  setTimeout(function () {
    clearInterval(splash);

    const randNumber = Math.round(getRandomArbitrary(1, max));
    resultDiv.style.fontWeight = "bold";
    resultDiv.value = `Игрок под номером ${randNumber} проиграл!`;
  }, 4000);
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}
